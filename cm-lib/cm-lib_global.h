#ifndef CMLIB_GLOBAL_H
#define CMLIB_GLOBAL_H

#pragma once
//#include <QtCore/qglobal.h>
#include <QtCore/QtGlobal>

#if defined(CMLIB_LIBRARY)
#  define CMLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CMLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CMLIB_GLOBAL_H
